#include "longe.h"

using namespace std;

longe::longe( const int a ) {
	int na=0;// ������� ��� ����������� �������
	int d=MOD,g=1,k=1;// g � l - ���������� ��� ��������� ��������� �������
	int a1=abs(a);	
	
	if (a>=0) sign=1; 
    else sign=-1;
	
	while ( ((abs(a))/d)>0 ) {       
		na++;
		d=d*MOD;		
	}
	
    size = na+1;
	mas = new int[size];
	//mas[na]=0;
	
    for (int i=0;i<size;i++) {
		g=1;
		k=1;
		for (int j=0;j<i;j++) g=g*MOD;
		for (int j=0;j<=i;j++) a1=a1/MOD;
		for (int j=0;j<=i;j++) a1=a1*MOD;

		mas[i]=(((abs(a))/g)*g-a1)/g;
	}
}

longe::longe( const char* a ) {
 
    int offset=0,k=0;// offset - �������� ��-�� ������. k - ����� ��� ������� � ������� �����
    char **str;//������ �����, �� ������� ���� ������� ������� ������
    
    if (a[0]=='-') {
        offset=1;
        sign=-1;
    } else sign=1;
    
    if (((strlen(a)-offset)%4)>0)
       size=(strlen(a)-offset)/4+1;
    else
       size=(strlen(a)-offset)/4;

    str=new char* [size];
 
    for (int i=0;i<size;i++)
        str[i]=new char[5];
   
	mas = new int[size];        
    for (int i=0;i<size;i++)
        mas[i]=0;
           

    for (int i=strlen(a)-4;i>=-3-offset;i=i-4) {
        
        if (i<offset) {
        
           for (int j=offset;j<=3+i;j++)
           {
               str[k][j-offset]=a[j];
               if (j==3+i) str[k][3+i+1]='\0';
           }
        } else {
               
            for (int j=i;j<=3+i;j++)
            {
                str[k][j-i]=a[j];
                if (j==3+i) str[k][4]='\0';
            }
        }
        k++;
    }
            
    for (int i=0;i<size;i++)
        mas[i]=atoi(str[i]);            
}

longe::~longe() {
                
	delete []mas;
	
}

longe& longe :: operator = ( const longe& b ) {
       
	size=b.size;
	sign=b.sign;
	mas = new int [size];
	
	for (int i=0;i<size;i++)
		mas[i]=b.mas[i];

	return *this;

}
/* ��� �������� ������� print() - ���������� ������� ����� ��� �����, 
      � printall() - ���������� ������� ����� ����������
      (����������� ������).

void longe :: print() {
     
	int flag=0,d,help,fl=size+1;
	if (sign==-1)
	std::cout << '-' ;
	
	for (int i=size-1;i>=0;i--) {
        
		d=10;
		if ((mas[i]==0) && (flag==0)) {	
                        		
			if (i==0) 
				std::cout << '0'<< '\n';
				
			fl=i;
			continue;
		}
		flag=1;
		if (i<fl-2) {
                    
			if (mas[i]==0) 
				help=1;
			else
				help=mas[i];
				
			while ((help*d)<MOD) {
				d=d*10;
				std::cout << '0';
				
			}
		}
		
		std::cout << mas[i];
	}
	
	std::cout << '\n';
	
}
*/
void longe :: printall() {
	
	if (sign==-1)
	   std::cout << '-' ;
	
	for (int i=size-1;i>=0;i--)
		std::cout << mas[i] << "  ";
 
	std::cout << '\n';
	
}


istream& operator >> ( istream& in, longe& b) {
         
    string s;
    in >> s;
    b=longe(s.c_str()); 
    return in;
    
}

ostream& operator << ( ostream& out, const longe& b) {
         
    int flag=0,d,help,fl=b.size;
//flag �����,����� �������� ������ ������ �������
//     ��������(������ 00 001 234 - ���������� 1234)
//fl ��������� �� ��������� �� ������ �������
//   ��������(������ 0 000 040 345 - fl==1)

	if (b.sign==-1)
	out << '-' ;
	
	for (int i=b.size-1;i>=0;i--) {
        
		d=10;
		
		if ((b.mas[i]==0) && (flag==0)) {	
                          		
			if (i==0) 
				out << '0'<< '\n';
			fl=i;
			continue;
		}
		
		flag=1;
		
		if (i<fl-1) {
                    
			if (b.mas[i]==0) 
				help=1;
			else
				help=b.mas[i];
				
			while ((help*d)<MOD) {
                  
				d=d*10;
				out << '0';
			}
		}
		out << b.mas[i];
	}
	out << '\n';
         
    return out;
}

longe longe::operator-() {
      
	longe b;
	b.size=size;
	
	for(int i=0;i<b.size;i++)
		b.mas[i]=mas[i];
	
	if (sign==1)
		b.sign=-1;
	else 
        b.sign=1;
	
	return b;
}

bool longe :: operator > ( const longe& b) {
     
	if (sign>b.sign) return true;
	else if (sign<b.sign) return false;
	else { 
	    if (size>b.size) return true;
		else if (size<b.size) return false;
		else { 
             
			for (int i=size-1;i>=0;i--) {
				if (mas[i]==b.mas[i]) continue;
				if (mas[i]>b.mas[i]) return true;
				if (mas[i]<b.mas[i]) return false;
			}
			return false;
		}
	}	
}

bool longe :: operator < ( const longe& b) {
     
	if (sign<b.sign) return true;
	else if (sign>b.sign) return false;
	else { 
		if (size<b.size) return true;
		if (size>b.size) return false;
		else { 
			for (int i=size-1;i>=0;i--) {
				if (mas[i]==b.mas[i]) continue;
				if (mas[i]<b.mas[i]) return true;
				if (mas[i]>b.mas[i]) return false;
			}

			return false;
		}
	}		
}

bool longe :: operator >= ( const longe& b) {
     
	if (sign>b.sign) return true;
	else if (sign<b.sign) return false;
	else { 
		if (size>b.size) return true;
		if (size<b.size) return false;
		else { 
			for (int i=size-1;i>=0;i--) {
				if (mas[i]==b.mas[i]) continue;
				if (mas[i]>b.mas[i]) return true;
				if (mas[i]<b.mas[i]) return false;
			}
			return true;
		}
	}		
}

bool longe :: operator <= ( const longe& b) {
	if (sign<b.sign) return true;
	else if (sign>b.sign) return false;
	else { 
		if (size<b.size) return true;
		if (size>b.size) return false;
		else { 
			for (int i=size-1;i>=0;i--)
			{
				if (mas[i]==b.mas[i]) continue;
				if (mas[i]<b.mas[i]) return true;
				if (mas[i]>b.mas[i]) return false;
			}
			return true;
		}
	}		
}

bool longe :: operator == ( const longe& b) {
	if (sign<b.sign) return false;
	else if (sign>b.sign) return false;
	else { 
		if (size<b.size) return false;
		if (size>b.size) return false;
		else { 
			for (int i=size-1;i>=0;i--)
			{
				if (mas[i]!=b.mas[i]) return false;
			}
			return true;
		}
	}		
}

bool longe :: operator != ( const longe& b) {
	if (sign<b.sign) return true;
	else if (sign>b.sign) return true;
	else { 
		if (size<b.size) return true;
		if (size>b.size) return true;
		else { 
			for (int i=size-1;i>=0;i--)
			{
				if (mas[i]!=b.mas[i]) return true;
				
			}
			return false;
		}
	}		
}

longe longe :: operator + ( const longe& b) {
	longe ans;
	int minsize,idue=0,torb=1;
	int va=1,vb=1;
	
	for (int i=size-1;i>=0;i--) {
        
		if (mas[i]!=0) {
			va=i+1;
			break;
		}
	}

	for (int i=b.size-1;i>=0;i--) {
		
        if (b.mas[i]!=0) {
			vb=i+1;
			break;
		}
	}

	if (va>vb) {
			ans.size=size+1;
			minsize=vb;
	}
	else if  (va<vb) {
		ans.size=vb+1;
		minsize=va;
		torb=-1;
	}
	else {
		ans.size=va+1;
		minsize=va;
		for (int i=va-1;i>=0;i--) {
				if (mas[i]==b.mas[i]) continue;
				if (mas[i]>b.mas[i]) break;
				if (mas[i]<b.mas[i]) {
					torb=-1;
					break;
				}

			}
	
	}

	ans.mas = new int [ans.size];

	for (int i=0;i<ans.size;i++)
		ans.mas[i]=0;

		if (sign==b.sign) {
			for (int i=0;i<minsize;i++) {
				ans.mas[i]=mas[i]+b.mas[i]+idue;
				if ((idue=(ans.mas[i]/MOD))>=1)
					ans.mas[i]=ans.mas[i]-idue*MOD;
			}
			
			if (torb==1) {
				for (int i=minsize;i<va;i++) {
					ans.mas[i]=mas[i]+idue;
					if ((idue=(ans.mas[i]/MOD))>=1)
						ans.mas[i]=ans.mas[i]-idue*MOD;
				}
			}
			if (torb==-1) {
				for (int i=minsize;i<vb;i++) {
					ans.mas[i]=b.mas[i]+idue;
					if ((idue=(ans.mas[i]/MOD))>=1)
						ans.mas[i]=ans.mas[i]-idue*MOD;
				}
			}
			ans.mas[ans.size-1]=ans.mas[ans.size-1]+idue;
			ans.sign=sign;
		}
		else {
			
			if (torb==1) {
				for (int i=minsize;i<va;i++) {
					ans.mas[i]=mas[i];
				}
				
				for (int i=0;i<minsize;i++) {   		
					if (mas[i]-b.mas[i]<0) {
						ans.mas[i]=ans.mas[i]+MOD+mas[i]-b.mas[i];
						ans.mas[i+1]=ans.mas[i+1]-1;
					}
					else	
						ans.mas[i]=ans.mas[i]+mas[i]-b.mas[i];
				}
				ans.sign=sign;
			}

			if (torb==-1) {
				for (int i=minsize;i<vb;i++) {
					ans.mas[i]=b.mas[i];
				}
				
				for (int i=0;i<minsize;i++) {   		
					if (b.mas[i]-mas[i]<0) {
						ans.mas[i]=ans.mas[i]+MOD+b.mas[i]-mas[i];
						ans.mas[i+1]=ans.mas[i+1]-1;
					}
					else	
						ans.mas[i]=ans.mas[i]+b.mas[i]-mas[i];
				}
				ans.sign=b.sign;
			}
		}

int counter=0;	

    for (int i=ans.size-1;i>=0;i--) {
        if ((ans.mas[i]==0) && (i==0)) break;
        if (ans.mas[i]!=0) {
        break;
        }
        counter-=1;
    }


ans.size=ans.size+counter;

longe z;
z.size=1;
z.sign=-1;
z.mas[0]=0;

if (ans==z) 
	ans.sign=1;


return ans;
}

longe longe :: operator - ( const longe& b) {
	longe ans;
	int minsize,idue=0,torb=1;
	int va=1,vb=1;
	
	for (int i=size-1;i>=0;i--) {
		if (mas[i]!=0) {
			va=i+1;
			break;
		}
	}

	for (int i=b.size-1;i>=0;i--) {
		if (b.mas[i]!=0) {
			vb=i+1;
			break;
		}
	}
	
	if (va>vb) {
			ans.size=va+1;
			minsize=vb;
	}
	else if (va<vb) {
		ans.size=vb+1;
		minsize=va;
		torb=-1;
	}
    
	else  {
		ans.size=va+1;
		minsize=va;
		for (int i=va-1;i>=0;i--) {
				if (mas[i]==b.mas[i]) continue;
				if (mas[i]>b.mas[i]) break;
				if (mas[i]<b.mas[i]) {
					torb=-1;
					break;
				}
			}
	}

	ans.mas = new int [ans.size];

	for (int i=0;i<ans.size;i++)
		ans.mas[i]=0;
	
		if (sign==b.sign) {
			
			if (torb==1) {

				for (int i=minsize;i<va;i++) {
					ans.mas[i]=mas[i];
				}
				 
				for (int i=0;i<minsize;i++) {   		
					if ((mas[i]-b.mas[i])<0) {
						ans.mas[i]=ans.mas[i]+MOD+mas[i]-b.mas[i];
						ans.mas[i+1]=ans.mas[i+1]-1;
					}
					else	
						ans.mas[i]=ans.mas[i]+mas[i]-b.mas[i];
				}
				ans.sign=sign;
			}

			if (torb==-1) {
				for (int i=minsize;i<vb;i++) {
					ans.mas[i]=b.mas[i];
				}
				
				for (int i=0;i<minsize;i++) {   	
					if (b.mas[i]-mas[i]<0) {
						ans.mas[i]=ans.mas[i]+MOD+b.mas[i]-mas[i];
						ans.mas[i+1]=ans.mas[i+1]-1;
					}
					else	
						ans.mas[i]=ans.mas[i]+b.mas[i]-mas[i];
				}
				ans.sign=b.sign;
			}
		}

		else {
			for (int i=0;i<minsize;i++) {
				ans.mas[i]=mas[i]+b.mas[i]+idue;
				if ((idue=(ans.mas[i]/MOD))>=1)
					ans.mas[i]=ans.mas[i]-idue*MOD;
			}
			if (torb==1) {
				for (int i=minsize;i<va;i++) {
					ans.mas[i]=mas[i]+idue;
					if ((idue=(ans.mas[i]/MOD))>=1)
						ans.mas[i]=ans.mas[i]-idue*MOD;
				}
			}
			if (torb==-1) {
				for (int i=minsize;i<vb;i++) {
					ans.mas[i]=b.mas[i]+idue;
					if ((idue=(ans.mas[i]/MOD))>=1)
						ans.mas[i]=ans.mas[i]-idue*MOD;
				}
			}
			ans.mas[ans.size-1]=ans.mas[ans.size-1]+idue;
			ans.sign=sign;
		}

int counter=0;	

    for (int i=ans.size-1;i>=0;i--) {
        if ((ans.mas[i]==0) && (i==0)) break;
        if (ans.mas[i]!=0) {
        break;
        }
        counter-=1;
    }

ans.size=ans.size+counter;

longe z;
z.size=1;
z.sign=-1;
z.mas[0]=0;
if (ans==z) 
	ans.sign=1;

return ans;
}


longe longe :: operator * ( const longe& b) {
	longe ans;
	int hv,um;
	int va=1,vb=1;
	
	for (int i=size-1;i>=0;i--) {
		if (mas[i]!=0) {
			va=i+1;
			break;
		}
	}

	for (int i=b.size-1;i>=0;i--) {
		if (b.mas[i]!=0) {
			vb=i+1;
			break;
		}
	}
	
	ans.size=va+vb;

	ans.mas = new int [ans.size];

	for (int i=0;i<ans.size;i++)
		ans.mas[i]=0;

	for (int i=0;i<vb;i++) {
		hv=0;
		for (int j=0;j<va;j++) {
            
			um=b.mas[i]*mas[j];
			ans.mas[j+i]=ans.mas[j+i]+um-(um/MOD)*MOD+hv;
			hv=um/MOD;
			
			if ((j==size-1) && (i!=b.size-1)) ans.mas[i+j+1]=ans.mas[i+j+1]+hv;
			
			if ((ans.mas[j+i]/MOD)>=1) {
			ans.mas[j+i+1]=ans.mas[j+i+1]+ans.mas[j+i]/MOD;
			ans.mas[j+i]=ans.mas[j+i]-(ans.mas[j+i]/MOD)*MOD;
			}
		}
	
	}

	if (hv!=0) ans.mas[ans.size-1]=ans.mas[ans.size-1]+hv;

	if (sign==b.sign) ans.sign=1;
	else ans.sign=-1;

int counter=0;	

    for (int i=ans.size-1;i>=0;i--) {
        if ((ans.mas[i]==0) && (i==0)) break;
        if (ans.mas[i]!=0) {
        break;
        }
        counter-=1;
    }

ans.size=ans.size+counter;

longe z;
z.size=1;
z.sign=-1;
z.mas[0]=0;
if (ans==z) 
	ans.sign=1;


return ans;
}


longe longe :: operator / ( const longe& b ) {
	
	longe ans,tmp;
	tmp.sign=sign;
	tmp.size=size;
	

	for ( int i=0;i<size;i++) {
		tmp.mas[i]=mas[i];
	}
	
	int counter=0,helpflag=0;
	int va=1,vb=1;
	
	for (int i=size-1;i>=0;i--) {
		if (mas[i]!=0) {
			va=i+1;
			break;
		}
	}

	for (int i=b.size-1;i>=0;i--) {
		if (b.mas[i]!=0) {
			vb=i+1;
			break;
		}
	}	


	for (int i=vb-1;i>=0;i--) {
        
		if (b.mas[i]!=0) break;
		
		if ((b.mas[i]==0) && (i==0)) {
			std::cout << "Divide By ZERO!" << std::endl;
			ans = 0;
			return ans;
		}
	}
	if (va>vb) {	
	    ans.size=va-vb+1;	
	}
	else if (va<vb) {
		ans.size=1;
		ans.sign=1;
		ans.mas[0]=0;
		return ans;	
	}
    
	else {
		for (int i=va-1;i>=0;i--) {
            
		    if (mas[i]==b.mas[i]) {
                                  
		        if (i==0) {
		            ans.size=1;
		            ans.sign=1;
		            ans.mas[0]=1;
		            return ans;
		        }
		        continue;
		    } else if (mas[i]>b.mas[i]) {
		        ans.size=va-vb+1;
		        break;
		    } else {
                ans.size=1;
		        ans.sign=1;
		        ans.mas[0]=0;
		        return ans;
		    }
		}
	}
	
	ans.mas = new int [ans.size];
	
	for (int i=0;i<ans.size;i++)
		ans.mas[i]=0;
	
	if (sign==b.sign) ans.sign=1;
	else ans.sign=-1;

	int i=va-1,k=vb-1,flag=0;
	
	
	while (i>=vb-1) {
		if (mas[i]==0) {
			if (i==vb) {
				for (int j=i-1;j>=0;j--) {
					if (mas[j]==b.mas[k]) {
						k--;
						continue;
					}
					if (mas[j]>b.mas[k]) 
						break;
					if (mas[j]<b.mas[k]) {
						flag=1;
						break;
					}
				}
			}
			if (flag==1) 
				break;
            i--;
			if (i<0) break;
  	            continue;
        }

        k=vb-1,flag=0;
		
 	    for (int j=i;j>i-vb;j--) {    
 	        if (mas[j]==b.mas[k]) {
                if (j==0) 
      		        break;
                k--;
                continue;          
            } else if (mas[j]>b.mas[k]) 
                break;          
            else {
                flag=1; 
				break;  
            }		     
        }
	
		if (flag==1) {
			helpflag=0;
			while (helpflag==0) {
                k=0;
			    for (int j=i-vb;j<=i-1;j++) {   
					
					if ((mas[j]-b.mas[k])<0) {
						mas[j]=mas[j]+MOD-b.mas[k];
						mas[j+1]=mas[j+1]-1;
						k++;	
					} else {
			        		mas[j]=mas[j]-b.mas[k];
						k++;
                   	}
           		}
           
                counter++;
    		   
    			if (mas[i]>0)
    				continue;
    			k=vb-1;
     		    for (int j=i-1;j>=i-vb;j--) {
     		        if (mas[j]==b.mas[k]) {
    				    k--;
    				    continue;          
    					} else if (mas[j]>b.mas[k]) break;
                        else {
    						helpflag=1; 
    						ans.mas[i-vb]=ans.mas[i-vb]+counter;
    						counter=0;
    						break;
    				     	}
    		     }
             	
			}

		}
		
		if (flag==0) {       
			helpflag=0;
           	while (helpflag==0) {
           		k=0;
           		for (int j=i-vb+1;j<=i;j++) {   		
					if ((mas[j]-b.mas[k])<0) {
						mas[j]=mas[j]+MOD-b.mas[k];
						mas[j+1]=mas[j+1]-1;
						k++;
					} else {
						mas[j]=mas[j]-b.mas[k];
						k++;
					}
           		}
           
           		counter++;
           		k=vb-1;
           		for (int j=i;j>i-vb;j--) {
            		if (mas[j]==b.mas[k]) {
						if (j==i-vb+1)
						    break;
						k--;
						continue;          
                    } else if (mas[j]>b.mas[k]) break;
             		else {
                         	helpflag=1; 
    						ans.mas[i-vb+1]=ans.mas[i-vb+1]+counter;
    						counter=0;
    						break;
             			}
                }	
         	}
      	}

	i=size-1;flag=0;
	}

for ( int i=0;i<size;i++) {
		mas[i]=tmp.mas[i];
	}
	
int counterr=0;	

    for (int i=ans.size-1;i>=0;i--) {
        if ((ans.mas[i]==0) && (i==0)) break;
        if (ans.mas[i]!=0) {
        break;
        }
        counterr-=1;
    }

ans.size=ans.size+counterr;

longe z;
z.size=1;
z.sign=-1;
z.mas[0]=0;
if (ans==z) 
	ans.sign=1;


return ans;
}

longe longe :: operator % ( const longe& b) {
	longe ans;
	ans.sign=1;
	ans.size=size;
	ans=*this-(*this/b)*b;
	return ans;
}
/*
pair <longe,longe> longe :: operator divmod ( const longe& b) {
	longe ans1,ans2;
	ans1.sign=1;
	ans1.size=size;
	ans2.sign=1;
	ans2.size=size;
	ans1=*this/b;
	ans2=*this%b;
	return make_pair(ans1,ans2);
}
*/
longe& longe :: operator += ( const longe& b) {
	*this=*this+b;
	return *this;
}

longe& longe :: operator -= ( const longe& b) {
	*this=*this-b;
	return *this;
}

longe& longe :: operator *= ( const longe& b) {
	*this=*this*b;
	return *this;
}

longe& longe :: operator /= ( const longe& b) {
	*this=*this/b;
	return *this;
}
