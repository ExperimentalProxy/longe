#include <cstdlib>
#include <string.h>

#include <utility>
#include <iostream>
#include <string>

#define MOD 10000

using namespace std;

class longe
{
	int *mas;
	int sign;
	int size;
public:
	longe( const int =  0);
	longe( const char*);
	~longe();
	
	longe operator + ( const longe& );
	longe operator - ( const longe& );
	longe operator * ( const longe& );
	longe operator / ( const longe& );
	longe operator % ( const longe& );
	//pair< longe, longe > operator divmod ( const longe& );
	longe operator - ();
	bool operator > ( const longe& );
	bool operator < ( const longe& );
	bool operator >= ( const longe& );
	bool operator <= ( const longe& );
	bool operator == ( const longe& );
	bool operator != ( const longe& );
	longe& operator = ( const longe& );
	friend istream& operator >> ( istream&, longe& );
	friend ostream& operator << ( ostream&, const longe& );
	longe& operator += ( const longe& );
	longe& operator -= ( const longe& );
	longe& operator *= ( const longe& );
	longe& operator /= ( const longe& );
	void printall();
	void print ();
};



