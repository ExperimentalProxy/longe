#ifndef TEST_H
#define TEST_H

#include <cppunit/extensions/HelperMacros.h>
#include "longe.h"

using namespace CppUnit;

class LongTest : public TestFixture {
	CPPUNIT_TEST_SUITE(LongTest);

	CPPUNIT_TEST(Addition);
	CPPUNIT_TEST(Subtraction);
	CPPUNIT_TEST(Multiplication);
	CPPUNIT_TEST(Division);
	CPPUNIT_TEST(More);
	CPPUNIT_TEST(Less);
	CPPUNIT_TEST(MoreEqual);
	CPPUNIT_TEST(LessEqual);
	CPPUNIT_TEST(Equal);
	CPPUNIT_TEST(NotEqual);

	CPPUNIT_TEST_SUITE_END();
public:
       
	void setUp();
	void tearDown();
    	
	void Addition();
	void Subtraction();
	void Multiplication();
	void Division();
	void More();
	void Less();
	void MoreEqual();
	void LessEqual();
	void Equal(); 
	void NotEqual(); 
private:
	longe *a,*b,*c;

};


#endif //TEST_H
